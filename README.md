# Systém rozdělování vakcíny Moderna v Praze

## Cíl
Vytvořit systém, který doporučí optimální rozdělení vakcíny Moderna mezi jednotlivá očkovací místa (OČM), a který bude:

- Férový: bude zohledňovat stanovené prioritní skupiny a poměrně rozdělovat dávky vakcíny dle počtu čekajících v daném OČM
- Efektivní: bude  systematicky zpracovávat potřebu druhých dávek a kapacitu OČM
- Automatický: bude vyžadovat pouze minimální ruční zásahy a využívat vždy aktuální data

## Shrnutí

Systém funguje následujícím způsobem:

- Na denní bázi čerpáme data z otevřených datových sad na [API MZČR](https://onemocneni-aktualne.mzcr.cz/api/v2/covid-19).
    -  více informací v sekci "Popis datových zdrojů"
- V naší databázi tato data dál zpracováváme. 
    - Transformace najdete ve složce `data_transformation` jako SQL definice.
- S každou novou dodávkou vakcíny Moderna generujeme automatický report rozdělení (formát MS Excel/LibreOffice Calc), který předáváme Odboru zdravotnictví MHMP a asistujeme u jeho finalizace.
    - Příklad tohoto reportu najdete v souborech `priklad_rozdeleni_report.xlsx` a `priklad_rozdeleni_report.ods` 

## Popis výpočtu rozdělení

Výpočet postupuje v následujícím pořadí:

1. Pro každé OČM nejprve zohledníme potřebu pro druhé dávky (jedná se o odhad podle vykázaných očkování v systému ISIN), počítáme s 28 dny od první dávky.

2. Následně zohledníme počet osob v jednotlivých prioritních skupinách v následujícím pořadí , kteří se registrovali, ale ještě nemají termín (zdroj dat je CRS a Reservatic):
    - 80+
    - zdravotníci
    - 70-79
    - pedagogové
    - osoby s chronickým onemocněním či zdravotním stavem s vysokým rizikem těžkého průběhu covid-19

    V případě, že je vakcíny nedostatek na uspokojení některého z požadavků v bodě 2., rozdělíme dávky tak, abychom v každém OČM uspokojili stejný podíl skupiny čekajících.

Výsledkem je doporučení počtu dávek pro každé OČM na jednotky dávek. Následují dvě korekce:

1. Podle maximálních kapacit OČM odebereme dávky tam, kde počet dávek přesahuje maximální možnou kapacitu. Zbytek rozdělíme mezi ostatní místa s volnou kapacitou, opět poměrně k jejich počtu čekajících. Kapacity OČM sledujeme v kalendáři systému Reservatic, příp. se řídíme instrukcemi Odboru zdravotnictví MHMP.
    
2. Protože dodávky nelze dělit na menší počty než plata po 100 dávkách, musíme provést další korekci. Všechna místa s menším počtem než 100, dostanou jedno plato. U zbytku postupujeme tak, abychom ubrali/přidali co nejmenší počet vakcín oproti doporučenému rozdělení.

### Předpoklady
- Kvůli současné nepřesnosti skladových dat uvažujeme, že každé OČM bylo schopné vyočkovat dávky, které mu byly přiděleny v minulém období
- Uvažujeme, že počet registrovaných, kteří ještě nemají rezervaci (buď ji ještě neprovedli, nebo jsou "před závorou"), je dobrým indikátorem reálné fronty na očkování v daném OČM.
- Počítáme krátkodobý plán na 7 nebo 14 dnů dopředu a výpočet nezohledňuje případné výpadky dodávek, nebo potřebu druhých dávek v delším časovém horizontu. 

## Popis datových zdrojů

Pro výpočet čerpáme následující datové sady z [API MZČR](https://onemocneni-aktualne.mzcr.cz/api/v2/covid-19):

- COVID-19: Očkovací místa v ČR ([popis](https://onemocneni-aktualne.mzcr.cz/api/v2/covid-19/prehled-ockovacich-mist.csv-metadata.json))
    - Transformace dat: `data_transformation\v_uzis_covid19_vaccination_moderna_distribution.sql`

- COVID-19: Přehled vykázaných očkování podle očkovacích míst ČR ([popis](https://onemocneni-aktualne.mzcr.cz/api/v2/covid-19/ockovaci-mista.csv-metadata.json))
    - Transformace dat: `data_transformation\v_uzis_covid19_vaccination_moderna_estimated_second_dose_need.sql`

- COVID-19: Přehled registrací podle očkovacích míst ČR ([popis](https://onemocneni-aktualne.mzcr.cz/api/v2/covid-19/ockovani-registrace.csv-metadata.json))
    - Transformace dat: `data_transformation\v_uzis_covid19_vaccination_registration_reservation.sql`

## Kontakt

Pokud byste tento systém chtěli využít ve vašem kraji, nebo měli otázky a podněty, neváhejte nás kontaktovat.

Kontaktní osoba: František Kaláb (kalab.frantisek@operatorict.cz), Operátor ICT a.s.

/*
PostgreSQL 12.3

Spočítá počty registrací, rezervací a počet lidí, kteří ještě nemají rezervaci pro dané OČM podle prioritních skupin.
Výpočet zohledňuje stanovené priority a přiřazuje každého do jeho nejvyšší prioritní skupiny:
    - např. pokud je člověk nad 80 let věku a zároveň zdravotník, započítá se pouze jako 80+ (kategorie s vyšší prioritou)
*/
CREATE OR REPLACE VIEW analytic.v_uzis_covid19_vaccination_registration_reservation
AS SELECT s.ockovaci_misto_id,
    s.ockovaci_misto_nazev,
    s.kraj_nazev,
    s.updated_at,
    s.registrace_celkovy_pocet,
    s.registrace_80_plus,
    s.registrace_zdravotniku,
    s.registrace_70_79,
    s.registrace_pedagogu_nad_55,
    s.registrace_pedagogu_ostatni,
    s.registrace_chnronicky_nemocni,
    s.registrace_kriticka_infrastruktura,
    s.registrace_60_69,
    s.registrace_40_59,
    s.rezervace_celkovy_pocet,
    s.rezervace_80_plus,
    s.rezervace_zdravotniku,
    s.rezervace_70_79,
    s.rezervace_pedagogu_nad_55,
    s.rezervace_pedagogu_ostatni,
    s.rezervace_chnronicky_nemocni,
    s.rezervace_kriticka_infrastruktura,
    s.rezervace_60_69,
    s.rezervace_40_59,
    s.registrace_celkovy_pocet - s.rezervace_celkovy_pocet AS cekajicich_celkovy_pocet,
    s.registrace_80_plus - s.rezervace_80_plus AS cekajicich_80_plus,
    s.registrace_zdravotniku - s.rezervace_zdravotniku AS cekajicich_zdravotniku,
    s.registrace_70_79 - s.rezervace_70_79 AS cekajicich_70_79,
    s.registrace_pedagogu_nad_55 - s.rezervace_pedagogu_nad_55 AS cekajicich_pedagogu_nad_55,
    s.registrace_pedagogu_ostatni - s.rezervace_pedagogu_ostatni AS cekajicich_pedagogu_ostatni,
    s.registrace_chnronicky_nemocni - s.rezervace_chnronicky_nemocni AS cekajicich_chronicky_nemocnych,
    s.registrace_kriticka_infrastruktura - s.rezervace_kriticka_infrastruktura AS cekajicich_kriticka_infrastruktura,
    s.registrace_60_69 - s.rezervace_60_69 AS cekajicich_60_69,
    s.registrace_40_59 - s.rezervace_40_59 AS cekajicich_40_59
   FROM ( SELECT reg.ockovaci_misto_id,
            reg.ockovaci_misto_nazev,
            reg.kraj_nazev,
            reg.updated_at,
            count(*) AS registrace_celkovy_pocet,
            count(*) FILTER (WHERE reg.vekova_skupina = '80+'::text) AS registrace_80_plus,
            count(*) FILTER (WHERE reg.vekova_skupina <> '80+'::text AND reg.povolani = 'Zdravotnický pracovník dle §76 a §77 zákona 372/2011  Sb.'::text) AS registrace_zdravotniku,
            count(*) FILTER (WHERE reg.povolani <> 'Zdravotnický pracovník dle §76 a §77 zákona 372/2011  Sb.'::text AND (reg.vekova_skupina = ANY (ARRAY['70-74'::text, '75-79'::text]))) AS registrace_70_79,
            count(*) FILTER (WHERE reg.povolani = 'Pedagogický pracovník/nepedagogický zaměstnanec škol a školských zařízení/pečující osoba v dětských skupinách (mikrojeslích)/zaměstnanec v přímé péči zajišťující činnost zařízení pro děti vyžadující okamžitou pomoc'::text AND (reg.vekova_skupina = ANY (ARRAY['55-59'::text, '60-64'::text, '65-69'::text]))) AS registrace_pedagogu_nad_55,
            count(*) FILTER (WHERE reg.povolani = 'Pedagogický pracovník/nepedagogický zaměstnanec škol a školských zařízení/pečující osoba v dětských skupinách (mikrojeslích)/zaměstnanec v přímé péči zajišťující činnost zařízení pro děti vyžadující okamžitou pomoc'::text AND (reg.vekova_skupina <> ALL (ARRAY['55-59'::text, '60-64'::text, '65-69'::text, '70-74'::text, '75-79'::text, '80+'::text]))) AS registrace_pedagogu_ostatni,
            count(*) FILTER (WHERE reg.povolani = 'Osoba s chronickým onemocněním'::text AND (reg.vekova_skupina <> ALL (ARRAY['70-74'::text, '75-79'::text, '80+'::text]))) AS registrace_chnronicky_nemocni,
            count(*) FILTER (WHERE reg.povolani = 'Kritická infrastruktura'::text AND (reg.vekova_skupina <> ALL (ARRAY['70-74'::text, '75-79'::text, '80+'::text]))) AS registrace_kriticka_infrastruktura,
            count(*) FILTER (WHERE (reg.povolani <> ALL (ARRAY['Kritická infrastruktura'::text, 'Osoba s chronickým onemocněním'::text, 'Pedagogický pracovník/nepedagogický zaměstnanec škol a školských zařízení/pečující osoba v dětských skupinách (mikrojeslích)/zaměstnanec v přímé péči zajišťující činnost zařízení pro děti vyžadující okamžitou pomoc'::text, 'Zdravotnický pracovník dle §76 a §77 zákona 372/2011  Sb.'::text])) AND (reg.vekova_skupina = ANY (ARRAY['60-64'::text, '65-69'::text]))) AS registrace_60_69,
            count(*) FILTER (WHERE (reg.povolani <> ALL (ARRAY['Kritická infrastruktura'::text, 'Osoba s chronickým onemocněním'::text, 'Pedagogický pracovník/nepedagogický zaměstnanec škol a školských zařízení/pečující osoba v dětských skupinách (mikrojeslích)/zaměstnanec v přímé péči zajišťující činnost zařízení pro děti vyžadující okamžitou pomoc'::text, 'Zdravotnický pracovník dle §76 a §77 zákona 372/2011  Sb.'::text])) AND (reg.vekova_skupina = ANY (ARRAY['40-44'::text, '45-49'::text, '50-54'::text, '55-59'::text]))) AS registrace_40_59,
            count(*) FILTER (WHERE reg.rezervace = 1::double precision) AS rezervace_celkovy_pocet,
            count(*) FILTER (WHERE reg.rezervace = 1::double precision AND reg.vekova_skupina = '80+'::text) AS rezervace_80_plus,
            count(*) FILTER (WHERE reg.rezervace = 1::double precision AND reg.vekova_skupina <> '80+'::text AND reg.povolani = 'Zdravotnický pracovník dle §76 a §77 zákona 372/2011  Sb.'::text) AS rezervace_zdravotniku,
            count(*) FILTER (WHERE reg.rezervace = 1::double precision AND reg.povolani <> 'Zdravotnický pracovník dle §76 a §77 zákona 372/2011  Sb.'::text AND (reg.vekova_skupina = ANY (ARRAY['70-74'::text, '75-79'::text]))) AS rezervace_70_79,
            count(*) FILTER (WHERE reg.rezervace = 1::double precision AND reg.povolani = 'Pedagogický pracovník/nepedagogický zaměstnanec škol a školských zařízení/pečující osoba v dětských skupinách (mikrojeslích)/zaměstnanec v přímé péči zajišťující činnost zařízení pro děti vyžadující okamžitou pomoc'::text AND (reg.vekova_skupina = ANY (ARRAY['55-59'::text, '60-64'::text, '65-69'::text]))) AS rezervace_pedagogu_nad_55,
            count(*) FILTER (WHERE reg.rezervace = 1::double precision AND reg.povolani = 'Pedagogický pracovník/nepedagogický zaměstnanec škol a školských zařízení/pečující osoba v dětských skupinách (mikrojeslích)/zaměstnanec v přímé péči zajišťující činnost zařízení pro děti vyžadující okamžitou pomoc'::text AND (reg.vekova_skupina <> ALL (ARRAY['55-59'::text, '60-64'::text, '65-69'::text, '70-74'::text, '75-79'::text, '80+'::text]))) AS rezervace_pedagogu_ostatni,
            count(*) FILTER (WHERE reg.rezervace = 1::double precision AND reg.povolani = 'Osoba s chronickým onemocněním'::text AND (reg.vekova_skupina <> ALL (ARRAY['70-74'::text, '75-79'::text, '80+'::text]))) AS rezervace_chnronicky_nemocni,
            count(*) FILTER (WHERE reg.rezervace = 1::double precision AND reg.povolani = 'Kritická infrastruktura'::text AND (reg.vekova_skupina <> ALL (ARRAY['70-74'::text, '75-79'::text, '80+'::text]))) AS rezervace_kriticka_infrastruktura,
            count(*) FILTER (WHERE reg.rezervace = 1::double precision AND (reg.povolani <> ALL (ARRAY['Kritická infrastruktura'::text, 'Osoba s chronickým onemocněním'::text, 'Pedagogický pracovník/nepedagogický zaměstnanec škol a školských zařízení/pečující osoba v dětských skupinách (mikrojeslích)/zaměstnanec v přímé péči zajišťující činnost zařízení pro děti vyžadující okamžitou pomoc'::text, 'Zdravotnický pracovník dle §76 a §77 zákona 372/2011  Sb.'::text])) AND (reg.vekova_skupina = ANY (ARRAY['60-64'::text, '65-69'::text]))) AS rezervace_60_69,
            count(*) FILTER (WHERE reg.rezervace = 1::double precision AND (reg.povolani <> ALL (ARRAY['Kritická infrastruktura'::text, 'Osoba s chronickým onemocněním'::text, 'Pedagogický pracovník/nepedagogický zaměstnanec škol a školských zařízení/pečující osoba v dětských skupinách (mikrojeslích)/zaměstnanec v přímé péči zajišťující činnost zařízení pro děti vyžadující okamžitou pomoc'::text, 'Zdravotnický pracovník dle §76 a §77 zákona 372/2011  Sb.'::text])) AND (reg.vekova_skupina = ANY (ARRAY['40-44'::text, '45-49'::text, '50-54'::text, '55-59'::text]))) AS rezervace_40_59
           FROM uzis.covid19_vaccination_registrations reg
          GROUP BY reg.ockovaci_misto_id, reg.ockovaci_misto_nazev, reg.kraj_nazev, reg.updated_at
          ORDER BY (count(*)) DESC) s;

/*
PostgreSQL 12.3

Ze skladových dat získá celkové příjmy a výdaje očkovací látky pro jednotlivé OČM
a dopočítá počet dávek které mělo dané OČM k dispozici.
*/
CREATE OR REPLACE VIEW analytic.v_uzis_covid19_vaccination_moderna_received
AS 
    SELECT 
        d.ockovaci_misto_id,
        d.ockovaci_misto_nazev,
        d.kraj_nazev,
        d_prijato.prijato_od_jinych_ocm + sum(d.pocet_davek) FILTER (WHERE d.akce = 'Příjem') AS prijato_davek_moderna,
        COALESCE(sum(d.pocet_davek) FILTER (WHERE d.akce = 'Výdej'), 0) AS vydano_davek_moderna,
        d_prijato.prijato_od_jinych_ocm + sum(d.pocet_davek) FILTER (WHERE d.akce = 'Příjem') - 
            COALESCE(sum(d.pocet_davek) FILTER (WHERE d.akce = 'Výdej'), 0) AS davek_moderna_k_dispozici
    FROM uzis.covid19_vaccination_distribution d
        -- následující join dopočítá příjmy od ostatních OČM mimo centrálního skladu
   	    left join (
            SELECT 
                covid19_vaccination_distribution.cilove_ockovaci_misto_id,
                sum(covid19_vaccination_distribution.pocet_davek) AS prijato_od_jinych_ocm
            FROM uzis.covid19_vaccination_distribution
            WHERE 
               	covid19_vaccination_distribution.akce = 'Výdej'
                and covid19_vaccination_distribution.vyrobce = 'Moderna'
            GROUP BY 
                covid19_vaccination_distribution.cilove_ockovaci_misto_id
        ) d_prijato ON 
            d_prijato.cilove_ockovaci_misto_id = d.ockovaci_misto_id
    WHERE 
  	    d.vyrobce = 'Moderna'
    GROUP BY 
        d.ockovaci_misto_id, 
        d.ockovaci_misto_nazev, 
        d.kraj_nazev,
        d_prijato.prijato_od_jinych_ocm
;

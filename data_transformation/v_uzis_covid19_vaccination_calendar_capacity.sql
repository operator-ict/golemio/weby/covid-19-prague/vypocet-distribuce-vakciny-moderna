/*
PostgreSQL 12.3

Pro každé OČM spočítá volnou a maximální kapacitu v kalendáři na příštích 7 a 14 dnů
*/
CREATE OR REPLACE VIEW analytic.v_uzis_covid19_vaccination_calendar_capacity
AS 
    SELECT 
        c.ockovaci_misto_id,
        c.ockovaci_misto_nazev,
        c.kraj_nazev,
        sum(c.volna_kapacita) FILTER (WHERE c.datum >= CURRENT_DATE AND c.datum < (CURRENT_DATE + '7 days'::interval)) AS volna_kapacita_7d,
        sum(c.maximalni_kapacita) FILTER (WHERE c.datum >= CURRENT_DATE AND c.datum < (CURRENT_DATE + '7 days'::interval)) AS maximalni_kapacita_7d,
        sum(c.volna_kapacita) FILTER (WHERE c.datum >= CURRENT_DATE AND c.datum < (CURRENT_DATE + '14 days'::interval)) AS volna_kapacita_14d,
        sum(c.maximalni_kapacita) FILTER (WHERE c.datum >= CURRENT_DATE AND c.datum < (CURRENT_DATE + '14 days'::interval)) AS maximalni_kapacita_14d
    FROM 
        uzis.covid19_vaccination_calendar_capacities c
    GROUP BY 
        c.ockovaci_misto_id,
        c.ockovaci_misto_nazev,
        c.kraj_nazev
    ORDER BY 
        (sum(c.maximalni_kapacita) FILTER (
            WHERE c.datum >= CURRENT_DATE 
            AND c.datum < (CURRENT_DATE + '7 days'::interval)
        )) DESC
;

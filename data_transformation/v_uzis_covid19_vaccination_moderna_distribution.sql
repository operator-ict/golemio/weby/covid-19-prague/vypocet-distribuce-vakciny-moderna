/*
PostgreSQL 12.3

Vytvoří finální view přiřazením vypočítaných údajů z ostatních výpočtů ke každému registrovanému OČM
Výstup tohoto view je vstupem výsledného reportu v Excel/LibreOfficeCalc
*/
CREATE OR REPLACE VIEW analytic.v_uzis_covid19_vaccination_moderna_distribution
AS SELECT s.updated_at,
    s.kraj_nazev,
    p.ockovaci_misto_id,
    s.zarizeni_kod AS nrpzs_kod,
        CASE
            WHEN p.ockovaci_misto_nazev IS NULL THEN s.zarizeni_nazev
            ELSE p.ockovaci_misto_nazev
        END AS ockovaci_misto_nazev,
    p.ockovaci_misto_adresa,
    COALESCE(p.operacni_status, 0::double precision) AS operacni_status,
    r.registrace_celkovy_pocet,
    r.registrace_80_plus,
    r.registrace_zdravotniku,
    r.registrace_70_79,
    r.registrace_pedagogu_nad_55,
    r.registrace_pedagogu_ostatni,
    r.registrace_chnronicky_nemocni,
    r.registrace_kriticka_infrastruktura,
    r.registrace_60_69,
    r.registrace_40_59,
    r.rezervace_celkovy_pocet,
    r.rezervace_80_plus,
    r.rezervace_zdravotniku,
    r.rezervace_70_79,
    r.rezervace_pedagogu_nad_55,
    r.rezervace_pedagogu_ostatni,
    r.rezervace_chnronicky_nemocni,
    r.rezervace_kriticka_infrastruktura,
    r.rezervace_60_69,
    r.rezervace_40_59,
    r.cekajicich_celkovy_pocet,
    r.cekajicich_80_plus,
    r.cekajicich_zdravotniku,
    r.cekajicich_70_79,
    r.cekajicich_pedagogu_nad_55,
    r.cekajicich_pedagogu_ostatni,
    r.cekajicich_chronicky_nemocnych,
    r.cekajicich_kriticka_infrastruktura,
    r.cekajicich_60_69,
    r.cekajicich_40_59,
    s.potreba_druhych_davek_kalendarni_1,
    s.potreba_druhych_davek_kalendarni_2,
    s.potreba_druhych_davek_kalendarni_3,
    s.potreba_druhych_davek_kalendarni_4
   FROM uzis.covid19_vaccination_points p
    LEFT JOIN analytic.v_uzis_covid19_vaccination_registration_reservation r 
        ON p.ockovaci_misto_id = r.ockovaci_misto_id
    RIGHT JOIN analytic.v_uzis_covid19_vaccination_moderna_estimated_second_dose_need s 
        ON "left"(p.nrpzs_kod::text, length(p.nrpzs_kod::text) - 2) = "left"(s.zarizeni_kod::text, length(s.zarizeni_kod::text) - 2);
/*
PostgreSQL 12.3

Pro každé OČM spočítá celkový počet použitých dávek
*/
CREATE OR REPLACE VIEW analytic.v_uzis_covid19_vaccination_moderna_used
AS 
    SELECT 
        u.ockovaci_misto_id,
        u.ockovaci_misto_nazev,
        u.kraj_nazev,
        sum(u.pouzite_davky) AS celkem_pouzitych_davek
    FROM 
        uzis.covid19_vaccination_usage u
    WHERE 
        u.vyrobce = 'Moderna'
    GROUP BY 
        u.ockovaci_misto_id,
        u.ockovaci_misto_nazev,
        u.kraj_nazev
;

/*
PostgreSQL 12.3

Spočítá kolik druhých dávek bude dané zařízení očkovat v následujících kalendářních týdnech
*/
CREATE OR REPLACE VIEW analytic.v_uzis_covid19_vaccination_moderna_estimated_second_dose_need
AS SELECT v.updated_at,
    v.zarizeni_kod,
    v.zarizeni_nazev,
    v.kraj_nazev,
    count(*) FILTER (WHERE date_part('week'::text, v.datum_vakcinace) = date_part('week'::text, CURRENT_DATE + '7 days'::interval)) AS potreba_druhych_davek_kalendarni_1,
    count(*) FILTER (WHERE date_part('week'::text, v.datum_vakcinace) = date_part('week'::text, CURRENT_DATE + '14 days'::interval)) AS potreba_druhych_davek_kalendarni_2,
    count(*) FILTER (WHERE date_part('week'::text, v.datum_vakcinace) = date_part('week'::text, CURRENT_DATE + '21 days'::interval)) AS potreba_druhych_davek_kalendarni_3,
    count(*) FILTER (WHERE date_part('week'::text, v.datum_vakcinace) = date_part('week'::text, CURRENT_DATE + '28 days'::interval)) AS potreba_druhych_davek_kalendarni_4
   FROM ( SELECT v_1.updated_at::date AS updated_at,
            v_1.zarizeni_kod,
            v_1.zarizeni_nazev,
            v_1.kraj_nazev,
            v_1.poradi_davky,
                CASE
                    WHEN v_1.datum_vakcinace < '2021-04-01 00:00:00'::timestamp without time zone THEN v_1.datum_vakcinace + '21 days'::interval
                    ELSE v_1.datum_vakcinace + '42 days'::interval
                END AS datum_vakcinace
           FROM uzis.covid19_vaccination_regional_details v_1
          WHERE v_1.vakcina = 'COVID-19 Vaccine Moderna'::text AND v_1.poradi_davky = 1 AND v_1.zarizeni_kod <> '26775816015'::bigint AND v_1.kraj_nazev = 'Hlavní město Praha'::text OR v_1.zarizeni_kod = '24250287000'::bigint) v
  GROUP BY v.updated_at, v.zarizeni_kod, v.zarizeni_nazev, v.kraj_nazev;
